﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace Raoke10086.Wechat
{
    /// <summary>
    /// 身份扩展
    /// </summary>
    public static class IdentityExtension
    {
        /// <summary>
        /// 获取用户ID
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static Guid GetUserID(this IIdentity identity)
        {
            var id = Guid.Empty;
            Guid.TryParse((identity as ClaimsIdentity).FindFirst("id")?.Value, out id);
            return id;
        }

        /// <summary>
        /// 获取用户昵称
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="defaultName">默认名称</param>
        /// <returns></returns>
        public static string GetNickName(this IIdentity identity, string defaultName = "Hi")
        {
            var claim = (identity as ClaimsIdentity).FindFirst("nickname");
            return claim != null ?
               (string.IsNullOrWhiteSpace(claim.Value) ? defaultName : claim.Value)
               : defaultName;
        }

        /// <summary>
        /// 获取用户头像
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static string GetAvatar(this IIdentity identity)
        {
            return (identity as ClaimsIdentity).FindFirst("avatar")?.Value;
        }
    }
}