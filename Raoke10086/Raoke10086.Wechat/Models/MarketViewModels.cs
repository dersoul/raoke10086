﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Wechat.Models
{
    public class MarketIndexViewModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }
    }
}