﻿using Raoke10086.Infrastructure.Enums;
using Raoke10086.Service.Abstracts;
using Raoke10086.Service.Models;
using Raoke10086.Wechat.Models;
using Raoke10086.Wechat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Raoke10086.Wechat.Controllers
{
    /// <summary>
    /// 商城
    /// </summary>
    [RoutePrefix("shop")]
    public class ShopController : Controller
    {
        #region Init
        private readonly IProductSvc _productSvc;

        public ShopController(IProductSvc productSvc)
        {
            _productSvc = productSvc;
        }
        #endregion

        /// <summary>
        /// 商品商城
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 充值话费和流量
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("chongzhi")]
        public ActionResult Chongzhi()
        {
            return View();
        }

        /// <summary>
        /// 号码商城
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("haoma")]
        [AllowAnonymous]
        public ActionResult Haoma(ShopHaomaFilter filter)
        {
            //ViewBag.Citys = _productSvc.GetCity(Current.LCID, Current.CategoryConfig.Haoma);
            ViewBag.Citys = new List<CityInListDto>
            {
                new CityInListDto
                {
                    Code="cd",
                    Name="成都"
                },
                new CityInListDto
                {
                    Code="bj",
                    Name="北京"
                },
                new CityInListDto
                {
                    Code="hz",
                    Name="杭州"
                }
            };
            ViewBag.CityCode = filter.City??Current.CityCode;

            return View(new PageList<ProductInListDto>
            {
                Data = new ProductInListDto[] {
                    new ProductInListDto
                    {
                        Title = "18682759981",
                        Cover="https://g-search1.alicdn.com/img/bao/uploaded/i4/imgextra/i4/178450245380320249/TB2VeNFsXXXXXXnXpXXXXXXXXXX_!!0-saturn_solar.jpg_80x80.jpg",
                        Introduction="电信3G/4G手机卡电信手机号码电话卡上网流量卡0月租全国无漫游1",
                        OriginPrice=200m,
                        SalePrice=188m,
                        PostPrice=10m,
                        Stock=1,
                        
                    },
                    new ProductInListDto
                    {
                        Title = "17266251182",
                        Cover="https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/TB1LxaCKXXXXXcsXVXXXXXXXXXX_!!0-item_pic.jpg_80x80.jpg",
                        Introduction="60元话费 中国电信4G手机卡 上网卡电话卡手机号码 全国电信号卡",
                        OriginPrice=60m,
                        SalePrice=48m,
                        PostPrice=10m,
                        Stock=1,

                    },
                     new ProductInListDto
                    {
                        Title = "18883215121",
                        Cover="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/TB1wvPcKFXXXXaWXpXXXXXXXXXX_!!0-item_pic.jpg_80x80.jpg",
                        Introduction="广东联通3g手机卡上网卡纯流量卡电话卡号码卡靓号套餐4g卡0月租",
                        OriginPrice=100m,
                        SalePrice=88m,
                        PostPrice=10m,
                        Stock=0,

                    },
                      new ProductInListDto
                    {
                        Title = "18752655515",
                        Cover="https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/TB1NeXpKpXXXXcxXVXXXXXXXXXX_!!0-item_pic.jpg_80x80.jpg",
                        Introduction="泰国电话卡 泰国Happy卡 7天 无限流量手机卡上网卡4G/3G流量卡",
                        OriginPrice=150m,
                        SalePrice=149m,
                        PostPrice=10m,
                        Stock=0,

                    },
                }
            });
        }


        /// <summary>
        /// 商品详情
        /// </summary>
        /// <param name="id">商品ID</param>
        /// <returns></returns>
        [HttpGet, Route("detail/{id}")]
        [AllowAnonymous]
        public ActionResult Detail(Guid id)
        {
            var product = _productSvc.Get(User.Identity.GetUserID(), id);
            return View(new ShopDetailViewModel
            {
                Product= product
            });
        }

        /// <summary>
        /// 提交订单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("trade")]
        public ActionResult PostTrade(Guid productID, int number)
        {
            //var product = new ProductDto
            //{
            //    Title = "音络AUCTOPUS 全向麦克风 会议音视频系统 会议 电话机 全向麦克风 增强版 i-25w",
            //    SalePrice = 19.9m,

            //    PostPrice = 12m,

            //    Galleries = new[]
            //    {
            //        "http://m.360buyimg.com/n12/jfs/t2773/154/1082733912/243541/c2adf0d9/5733ee59N42fa1efc.jpg!q70.jpg",
            //        "http://m.360buyimg.com/n12/jfs/t2815/205/1093038629/245158/e574acaf/5733ee55N806501fd.jpg!q70.jpg",
            //        "http://m.360buyimg.com/n12/jfs/t2833/136/1104823277/245432/2207876e/5733ee4bN0d1cf3ce.jpg!q70.jpg"
            //    },
            //};

            return View(new ShopTradeViewModel
            {
                //Product = product,
                Number = number
            });
        }

        /// <summary>
        /// 订单详情
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("trade-detail")]
        public ActionResult TradeDetail()
        {
            return View();
        }
    }
}