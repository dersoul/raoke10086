﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Admin
{
    public class Current
    {
        /// <summary>
        /// 当地语言编码
        /// </summary>
        public static string LCID { get; set; }

        /// <summary>
        /// 默认城市CODE
        /// </summary>
        public static string CityCode { get; set; }
    }
}