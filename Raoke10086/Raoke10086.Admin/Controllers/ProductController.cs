﻿using Raoke10086.Service.Abstracts;
using Raoke10086.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raoke10086.Admin.Extensions;

namespace Raoke10086.Admin.Controllers
{
    [RoutePrefix("product")]
    public class ProductController : Controller
    {
        private readonly IToolkitSvc _toolkitSvc;
        private readonly IProductSvc _productSvc;


        public ProductController(IToolkitSvc toolkitSvc,IProductSvc productSvc)
        {
            _toolkitSvc = toolkitSvc;
            _productSvc = productSvc;
        }

        [HttpGet,Route("")]
        public ActionResult Index(ProductFilterOfPage model)
        {
            ViewBag.Categories = _productSvc.GetCategory(Guid.Parse("39F77A79-8290-459C-A9DB-3AE808DC8A07"));
            ViewBag.AllCities = _productSvc.GetAllCity(Current.LCID);
            return View(_productSvc.Get(User.Identity.GetUserID(), model));
        }

        [HttpGet,Route("editor/{id}"),Route("editor")]
        public ActionResult Editor(Guid? id)
        {
            ViewBag.Id = id;
            ViewBag.QiniuToken = _toolkitSvc.GetQiniuToken("image");
            ViewBag.Categories = _productSvc.GetCategory(Guid.Parse("39F77A79-8290-459C-A9DB-3AE808DC8A07"));
            ViewBag.AllCities = _productSvc.GetAllCity(Current.LCID);
            if(id.HasValue)
            {
                var product = _productSvc.Get(User.Identity.GetUserID(), id.Value);
                return View(new UpdateProductDto
                {
                    CategaryID = product.CategaryID,
                    City = product.City,
                    Cover = product.Cover,
                    Details = product.Details,
                    Galleries = product.Galleries,
                    Introduction = product.Introduction,
                    IsVirtual = product.IsVirtual,
                    Messages = product.Messages,
                    NoteMessage = product.NoteMessage,
                    OriginPrice = product.OriginPrice,
                    PostPrice = product.PostPrice,
                    SalePrice = product.SalePrice,
                    ServiceProvider = product.ServiceProvider,
                    Stock = product.Stock,
                    Tags = product.Tags,
                    Title = product.Title
                });
            }
            return View();
        }

        [HttpPost, Route("editor/{id}"), Route("editor")]
        public ActionResult Editor(Guid? id, UpdateProductDto model)
        {
            _productSvc.Update(User.Identity.GetUserID(), id, model);
            return RedirectToAction("Index");
        }
    }
}