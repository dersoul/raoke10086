﻿using System.Web;
using System.Web.Optimization;

namespace Raoke10086.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/simditor").Include(
                       "~/Scripts/simditor/module.js",
                       "~/Scripts/simditor/hotkeys.js",
                       "~/Scripts/simditor/uploader.js",
                       "~/Scripts/simditor/simditor.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/vendor.js",
                        "~/Scripts/app.js",
                        "~/Scripts/bootbox/bootbox.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/vendor.css",
                      "~/Content/css/app-red.css"));

            bundles.Add(new StyleBundle("~/Content/simditor").Include(
                     "~/Content/css/simditor.css"));
        }
    }
}
