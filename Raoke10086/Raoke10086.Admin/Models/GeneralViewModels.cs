﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raoke10086.Admin.Models
{
    public class MenuViewModel
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }

        public string Url { get; set; }

        public ICollection<MenuViewModel> SubMenus { get; set; }
    }
}