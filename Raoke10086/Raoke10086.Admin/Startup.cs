﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Raoke10086.Admin.Startup))]
namespace Raoke10086.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
