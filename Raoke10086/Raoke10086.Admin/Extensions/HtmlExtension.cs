﻿using Raoke10086.Admin.Models;
using Raoke10086.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Raoke10086.Admin.Extensions
{
    public static class HtmlExtension
    {
        public static MvcHtmlString Pager<T>(this HtmlHelper helper, PageList<T> pager, Uri url)
        {
            if (pager == null || pager.TotalPage == 1)
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                var baseUrl = url.PathAndQuery;
                if (!Regex.IsMatch(baseUrl, @"page=\d"))
                {
                    baseUrl += ((string.IsNullOrEmpty(url.Query) ? "?page=" : "&page=") + pager.Page);
                }
                var start = Math.Max(1, pager.Page - 5);
                var end = Math.Min(pager.TotalPage, start + 10);
                var prev = Math.Max(start, pager.Page - 1);
                var next = Math.Min(end, pager.Page + 1);
                var ul = new TagBuilder("ul");
                ul.AddCssClass("pagination");
                var list = new List<TagBuilder>();
                list.Add(GetPageItem(baseUrl, "Prev", prev));
                for (int i = start; i <= end; i++)
                {
                    list.Add(GetPageItem(baseUrl, Convert.ToString(i), i, pager.Page == i));
                }
                list.Add(GetPageItem(baseUrl, "Next", next));
                ul.InnerHtml = string.Join("", list);
                return MvcHtmlString.Create(ul.ToString());
            }
        }

        public static MvcHtmlString Menu(this HtmlHelper helper,List<MenuViewModel> menus,string id)
        {
            var nav = new TagBuilder("nav");
            nav.AddCssClass("menu");
            var ul = new TagBuilder("ul");
            ul.Attributes["id"] = "sidebar-menu";
            ul.AddCssClass("nav metismenu");
            foreach (var menu in menus)
            {
                bool selectd;
                ul.InnerHtml += GetMenuLi(menu,id,out selectd);
            }
            nav.InnerHtml = ul.ToString();
            return MvcHtmlString.Create(nav.ToString());
        }

        private static TagBuilder GetMenuLi(MenuViewModel menu, string id,out bool selectd)
        {
            selectd = false;
            var li = new TagBuilder("li");
            if (menu.ID == id)
            {
                selectd = true;
                li.AddCssClass("active");
            }
            var a = new TagBuilder("a");
            a.Attributes["href"] = menu.Url;
            if (!string.IsNullOrEmpty(menu.Icon))
            {
                var i = new TagBuilder("i");
                i.AddCssClass(menu.Icon);
                a.InnerHtml += i.ToString();
            }
            a.InnerHtml += menu.Name;

            if (menu.SubMenus != null && menu.SubMenus.Any())
            {
                var i = new TagBuilder("i");
                i.AddCssClass("fa arrow");
                a.InnerHtml += i.ToString();
                li.InnerHtml = a.ToString();

                var ul = new TagBuilder("ul");
                foreach (var subMenu in menu.SubMenus)
                {
                    ul.InnerHtml += GetMenuLi(subMenu, id,out selectd);
                    if(selectd)
                    {
                        li.AddCssClass("active open");
                    }
                }
                li.InnerHtml += ul.ToString();
            }
            else
            {
                li.InnerHtml = a.ToString();
            }

            return li;
        }
        private static TagBuilder GetPageItem(string url, string name, int page, bool active = false)
        {
            var li = new TagBuilder("li");
            if (active)
            {
                li.AddCssClass("active");
            }
            li.AddCssClass("page-item");
            var a = new TagBuilder("a");
            a.AddCssClass("page-link");
            a.MergeAttribute("href", Regex.Replace(url, @"page=\d", "page=" + page));
            a.SetInnerText(name);
            li.InnerHtml = a.ToString();
            return li;
        }
    }
}