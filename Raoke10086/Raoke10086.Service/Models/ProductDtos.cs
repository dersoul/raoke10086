﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Raoke10086.Service.Models
{
    /// <summary>
    /// 商品详情
    /// </summary>
    public class ProductDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 分类ID
        /// </summary>
        public Guid CategaryID { get; set; }

        /// <summary>
        /// 标签[JSON]
        /// [{"id":1,"name":"电信","style":"color:#428bca"},{"id":38,"name":"成都","style":"color:#428bca"}]
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover { get; set; }

        /// <summary>
        /// 商品封面图片集
        /// </summary>
        public string DataGalleries { get; set; }

        /// <summary>
        /// 商品封面图片集
        /// </summary>
        public string[] Galleries
        {
            get
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(DataGalleries);
            }
        }

        /// <summary>
        /// 商家提示
        /// </summary>
        public string NoteMessage { get; set; }

        /// <summary>
        /// 商品留言
        /// type[text|tel|email|date|time|id_no|image]
        /// [{"name":"手机号","type":"tel","multiple":"0","required":"1"}]
        /// </summary>
        public string Messages { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal OriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal SalePrice { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal PostPrice { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// 已售数量
        /// </summary>
        public int Sold { get; set; }

        /// <summary>
        /// 是否是虚拟商品
        /// </summary>
        public bool IsVirtual { get; set; }

        /// <summary>
        /// 服务提供商
        /// </summary>
        public ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 城市ID
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 商品状态
        /// </summary>
        public ProductState State { get; set; }

        /// <summary>
        /// 商品简介
        /// </summary>
        public string Introduction { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string DataDetails { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Details
        {
            get
            {
                return HttpUtility.HtmlDecode(DataDetails);
            }
        }
    }

    /// <summary>
    /// 商品列表
    /// </summary>
    public class ProductInListDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 标签[JSON]
        /// [{"id":1,"name":"电信","style":"color:#428bca"},{"id":38,"name":"成都","style":"color:#428bca"}]
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 分类ID
        /// </summary>
        public Guid CategoryID { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal OriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal SalePrice { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal PostPrice { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// 已售数量
        /// </summary>
        public int Sold { get; set; }

        /// <summary>
        /// 商品简介
        /// </summary>
        [StringLength(500)]
        public string Introduction { get; set; }

        /// <summary>
        /// 商品状态
        /// </summary>
        public ProductState State { get; set; }

    }

    /// <summary>
    /// 更新商品详情
    /// </summary>
    public class UpdateProductDto
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public Guid CategaryID { get; set; }

        /// <summary>
        /// 标签[JSON]
        /// [{"id":1,"name":"电信","style":"color:#428bca"},{"id":38,"name":"成都","style":"color:#428bca"}]
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        [StringLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// 是否是虚拟商品
        /// </summary>
        public bool IsVirtual { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        [StringLength(200)]
        public string Cover { get; set; }

        /// <summary>
        /// 商品封面图片集
        /// </summary>
        public string[] Galleries { get; set; }

        /// <summary>
        /// 商家提示
        /// </summary>
        [StringLength(1000)]
        public string NoteMessage { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal OriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal SalePrice { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal PostPrice { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// 商品简介
        /// </summary>
        [StringLength(500)]
        public string Introduction { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [MaxLength]
        public string Details { get; set; }

        /// <summary>
        /// 商品留言
        /// type[text|tel|email|date|time|id_no|image]
        /// [{"name":"手机号","type":"tel","multiple":"0","required":"1"}]
        /// </summary>
        public string Messages { get; set; }

        /// <summary>
        /// 服务提供商
        /// </summary>
        public ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
    }

    /// <summary>
    /// 商品筛选器
    /// </summary>
    public class ProductFilterOfPage:InPage
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 分类ID
        /// </summary>
        public ICollection<Guid> CategaryID { get; set; }
        
        /// <summary>
        /// 最低价格
        /// </summary>
        public decimal? LowestPrice { get; set; }

        /// <summary>
        /// 最高价格
        /// </summary>
        public decimal? HighestPrice { get; set; }

        /// <summary>
        /// 是否是虚拟商品
        /// </summary>
        public bool? IsVirtual { get; set; }

        /// <summary>
        /// 服务提供商
        /// </summary>
        public ICollection<ServiceProvider> ServiceProvider { get; set; }

        /// <summary>
        /// 城市ID
        /// </summary>
        public ICollection<int> CityID { get; set; }
    }

    /// <summary>
    /// 商品分类（列表中）
    /// </summary>
    public class CategoryInListDto
    {
        public Guid ID { get; set; }

        public string Name { get; set; }
    }

    /// <summary>
    /// 城市（列表中）
    /// </summary>
    public class CityInListDto
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string Fullname { get; set; }
    }

    /// <summary>
    /// 标签
    /// </summary>
    public class TagDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Style { get; set; }
    }
}
