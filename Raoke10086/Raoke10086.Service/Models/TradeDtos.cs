﻿using Raoke10086.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Models
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public class TradeDto
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        public string ID { get; set; }

        #region 商品冗余信息
        /// <summary>
        /// 商品ID
        /// </summary>
        public Guid ProductID { get; set; }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal ProductOriginPrice { get; set; }

        /// <summary>
        /// 商品售价
        /// </summary>
        public decimal ProductSalePrice { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        public string ProductTitle { get; set; }

        /// <summary>
        /// 商品封面
        /// </summary>
        public string ProductCover { get; set; }

        #endregion

        #region 商品数量和价格

        /// <summary>
        /// 商品数量
        /// </summary>
        public double Number { get; set; }


        /// <summary>
        /// 交易价格
        /// </summary>
        public decimal Price { get; set; }

        #endregion

        #region 买家信息
        /// <summary>
        /// 购买者ID
        /// </summary>
        public Guid BuyerID { get; set; }

        /// <summary>
        /// 买家留言
        ///  [{"name":"手机号","value":"18682759981"}]
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 买家备注
        /// </summary>
        public string Memo { get; set; }

        #endregion

        #region 快递信息

        /// <summary>
        /// 邮费
        /// </summary>
        public decimal Postage { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }

        /// <summary>
        /// 收件人电话
        /// </summary>
        public string ReceiverPhone { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceiverAddress { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        public string AirWayBillNo { get; set; }

        #endregion

        #region 状态和操作时间
        /// <summary>
        /// 订单状态
        /// </summary>
        public TradeState State { get; set; }

        /// <summary>
        /// 付款状态
        /// </summary>
        public PayState PayState { get; set; }

        /// <summary>
        /// 配送状态
        /// </summary>
        public ShipmentState ShipmentState { get; set; }

        /// <summary>
        /// 订单生成时间
        /// </summary>
        public DateTime GenerationTime { get; set; }

        /// <summary>
        /// 订单确认时间
        /// </summary>
        public DateTime? ConfirmTime { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PayTime { get; set; }

        /// <summary>
        /// 订单发货时间
        /// </summary>
        public DateTime? DeliveryTime { get; set; }

        /// <summary>
        /// 订单处理完成时间
        /// </summary>
        public DateTime? FinishTime { get; set; }
        #endregion
    }

    /// <summary>
    /// 提交订单
    /// </summary>
    public class PostTradeDto
    {
        /// <summary>
        /// 外部交易订单
        /// </summary>
        [StringLength(128)]
        public string ExternalID { get; set; }

        /// <summary>
        /// 商品ID
        /// </summary>
        public Guid ProductID { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        public double Number { get; set; }

        /// <summary>
        /// 买家备注
        /// </summary>
        public string Memo { get; set; }

        /// <summary>
        /// 商品留言
        /// type[text|tel|email|date|time|id_no|image]
        /// [{"name":"手机号","type":"tel","multiple":"0","required":"1"}]
        /// </summary>
        [StringLength(300)]
        public string Messages { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }

        /// <summary>
        /// 收件人电话
        /// </summary>
        public string ReceiverPhone { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceiverAddress { get; set; }
    }


    public class TradeFilter
    {
        public Guid[] ID { get; set; }
    }


    /// <summary>
    /// 确认收货
    /// </summary>
    public class ReceiptDto
    {
        public Guid TradeID { get; set; }
    }
}
