﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Models
{
    /// <summary>
    /// 输入的Page信息
    /// 继承此类推荐使用XXXOfPage命名
    /// </summary>
    public class InPage
    {

        public InPage()
        {
            Page = 1;
            PageSize = 10;
        }

        /// <summary>
        /// 指定页码
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 指定每页的记录数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 指定返回结果属性排序
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// 排序顺序(asc/desc)
        /// </summary>
        public string Order { get; set; }
    }

    /// <summary>
    /// 输出的Page信息
    /// </summary>
    public class PageList<T>
    {
        private int _pageSize;

        /// <summary>
        /// 每页显示的条数
        /// </summary>
        public int PageSize
        {
            get { return (_pageSize == 0) ? 10 : _pageSize; }
            set { _pageSize = value; }
        }

        /// <summary>
        /// 数据
        /// </summary>
        public ICollection<T> Data { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPage
        {
            get { return (TotalCount + ((PageSize) - 1)) / (PageSize); }
        }

        /// <summary>
        /// 每页记录条数
        /// </summary>
        public int Count { get { return Data != null ? Data.Count() : 0; } }

        /// <summary>
        /// 当前页码
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 总记录
        /// </summary>
        public int TotalCount { get; set; }
    }
}
