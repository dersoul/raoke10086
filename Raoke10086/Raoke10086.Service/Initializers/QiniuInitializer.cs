﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Initializers
{
    public static class QiniuInitializer
    {
        public static void Init()
        {
            Qiniu.Conf.Config.ACCESS_KEY = ConfigurationManager.AppSettings["Qiniu.ACCESS_KEY"];
            Qiniu.Conf.Config.SECRET_KEY = ConfigurationManager.AppSettings["Qiniu.SECRET_KEY"];
        }
    }
}
