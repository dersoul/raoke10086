﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 角色
    /// </summary>
    [Table("Roles", Schema = "dbo")]
    public class Role
    {
        /// <summary>
        /// Key
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Key { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [StringLength(50)]
        public string Name { get; set; }
    }
}
