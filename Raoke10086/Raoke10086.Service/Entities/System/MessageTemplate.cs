﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    [Table("MessageTemplates", Schema = "dbo")]
    public class MessageTemplate
    {
        [Key,StringLength(32)]
        public string ID { get; set; }

        /// <summary>
        /// 微信模板ID
        /// </summary>
        [StringLength(100)]
        public string WechatTemplateID { get; set; }

        /// <summary>
        /// 模板标题
        /// </summary>
        [StringLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// 模板内容
        /// </summary>
        [StringLength(500)]
        public string Content { get; set; }

        /// <summary>
        /// 目标链接
        /// </summary>
        [StringLength(200)]
        public string Url { get; set; }
    }
}
