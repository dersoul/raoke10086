﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 城市
    /// </summary>
    [Table("Cities", Schema = "dbo")]
    public class City
    {
        /// <summary>
        /// Local ID 
        /// </summary>
        [Key, Column(Order = 0)]
        public string LCID { get; set; }

        /// <summary>
        /// 城市编码
        /// </summary>
        [Key, Column(Order = 1)]
        public string Code { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 完整名称
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 父CODE
        /// </summary>
        public string ParentCode { get; set; }
    }
}
