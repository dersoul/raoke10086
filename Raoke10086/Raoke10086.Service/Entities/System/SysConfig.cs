﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 系统设置
    /// </summary>
    [Table("SysConfigs", Schema = "dbo")]
    public class SysConfig
    {
        /// <summary>
        /// Key
        /// </summary>
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Key { get; set; }

        /// <summary>
        /// 应用目标
        /// </summary>
        [Key, Column(Order = 1)]
        public Guid UserID { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
    }
}
