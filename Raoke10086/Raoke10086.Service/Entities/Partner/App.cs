﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// App
    /// </summary>
    [Table("Apps", Schema = "dbo")]
    public class App
    {
        /// <summary>
        /// App ID
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ID { get; set; }

        /// <summary>
        /// App密钥
        /// </summary>
        public string Secret { get; set; }

        /// <summary>
        /// 回调URL
        /// </summary>
        public string CallbackURL { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserID { get; set; }
    }
}
