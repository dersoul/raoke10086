﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Entities
{
    /// <summary>
    /// 提现记录
    /// </summary>
    [Table("Withdrawals", Schema = "dbo")]
    public class Withdrawal
    {
        /// <summary>
        /// 提现编号
        /// </summary>
        [Key, StringLength(32), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// 提现描述信息
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 提现生成时间
        /// </summary>
        public DateTime GenerationTime { get; set; }

        /// <summary>
        /// 提现付款时间
        /// </summary>
        public DateTime? TransferTime { get; set; }

        /// <summary>
        /// 微信订单号
        /// </summary>
        public string TransferPaymentNo { get; set; }

        /// <summary>
        /// 微信支付成功时间
        /// </summary>
        public DateTime? TransferPaymentTime { get; set; }

        /// <summary>
        /// 付款业务结果[SUCCESS/FAIL]
        /// </summary>
        public string TransferResultCode { get; set; }

        /// <summary>
        /// 付款错误代码
        /// </summary>
        public string TransferErrCode { get; set; }

        /// <summary>
        /// 付款错误代码描述
        /// </summary>
        public string TransferErrCodeDes { get; set; }
    }
}
