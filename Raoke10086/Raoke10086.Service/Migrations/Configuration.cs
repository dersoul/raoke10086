namespace Raoke10086.Service.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Raoke10086.Service.Entities.Raoke10086Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Raoke10086.Service.Entities.Raoke10086Context context)
        {
            InitUsers(context);
            InitCategory(context);
            context.SaveChanges();
        }

        private void InitUsers(Entities.Raoke10086Context context)
        {
            context.Roles.AddOrUpdate(
             role => role.Key,
             new Entities.Role { Key = "admin", Name = "管理员" },
             new Entities.Role { Key = "employee", Name = "员工" },
             new Entities.Role { Key = "member", Name = "普通会员" },
             new Entities.Role { Key = "distributor", Name = "分销商" },
             new Entities.Role { Key = "supplier", Name = "供应商" },
             new Entities.Role { Key = "agent", Name = "代理商" }
             );
            var adminID = Guid.Parse("2F71943C-E13B-46D8-B196-30DA391B6DAB");

            context.Users.AddOrUpdate(
               user => user.UserName,
               new Entities.User
               {
                   ID = adminID,
                   UserName = "admin",
                   Email = "admin@raoke10086.com",
                   State =Infrastructure.Enums.UserState.Activation,
                   EmailConfirmed = true,
                   Password = BCrypt.Net.BCrypt.HashPassword("amazing"),
                   JoinDate = DateTime.Now
               }
            );

            context.UserProfiles.AddOrUpdate(
             user => user.UserID,
             new Entities.UserProfile { UserID = adminID,Avatar= "http://admin.raoke10086.com/content/imgs/avatar.jpeg", Nickname = "南新 詹" }
            );

            context.UserRoles.AddOrUpdate(
             userRole => new { userRole.RoleKey, userRole.UserID },
             new Entities.UserRole { RoleKey = "admin", UserID = adminID }
            );
        }

        private void InitCategory(Entities.Raoke10086Context context)
        {
            //充值
            var chongzhi = Guid.Parse("999B60D3-9078-42FE-B80A-98F054ABBBA8");
            //商品
            var shangpin = Guid.Parse("39F77A79-8290-459C-A9DB-3AE808DC8A07");
            //号卡
            var haomaka = Guid.Parse("CD2D936A-0B16-465C-AC65-D7D0985DE966");

            #region 商品子分类
            var shangpin_shouji = Guid.Parse("D4929BE3-BD04-4F9E-A0FD-59CC96602BF7");
            var shangpin_shangwangka = Guid.Parse("2AD6492C-4963-4CD0-B2A5-6341C2D1A70B");
            var shangpin_shangwangshebei = Guid.Parse("8CF1852F-0183-45DC-9778-FB6B6619FBE7");
            var shangpin_yuyinhezi = Guid.Parse("8DBC83F5-57EF-4CA2-A569-63D610EE1BC7");
            var shangpin_maochi = Guid.Parse("371B2F2C-CBD2-473F-A2A6-024C15E7A43E");
            #endregion

            #region 充值子分类
            var chongzhi_huafei = Guid.Parse("2DB31323-6D01-4931-AB4D-2C78AC4C4495");
            var chongzhi_liuliang = Guid.Parse("5387BB1E-E810-4460-9322-CE3D036E1236");
            #endregion

            #region 号卡子分类
            var haomaka_haoma = Guid.Parse("32544BD4-2AF8-48F6-896C-AC310D072A58");
            var haomaka_yewu = Guid.Parse("0E35741E-C481-41D7-AA43-6E8B02713E92");
            #endregion

            context.Categories.AddOrUpdate(
                category => category.ID,
                new Entities.Category { ID = chongzhi, Name = "充值" },
                new Entities.Category { ID = shangpin, Name = "商品" },
                new Entities.Category { ID = haomaka, Name = "号卡" },
            #region 商品子分类
                new Entities.Category { ID = shangpin_shouji, Name = "手机", PID = shangpin },
                new Entities.Category { ID = shangpin_shangwangka, Name = "上网卡", PID = shangpin },
                new Entities.Category { ID = shangpin_shangwangshebei, Name = "上网设备", PID = shangpin },
                new Entities.Category { ID = shangpin_yuyinhezi, Name = "语音盒子", PID = shangpin },
                new Entities.Category { ID = shangpin_maochi, Name = "猫池", PID = shangpin },
            #endregion
            #region 商品子分类
                new Entities.Category { ID = chongzhi_huafei, Name = "话费", PID = chongzhi },
                new Entities.Category { ID = chongzhi_liuliang, Name = "流量", PID = chongzhi },
            #endregion
            #region 商品子分类
                new Entities.Category { ID = haomaka_haoma, Name = "号码卡", PID = haomaka },
                new Entities.Category { ID = haomaka_yewu, Name = "业务卡", PID = haomaka }
            #endregion
               );
        }
    }
}
