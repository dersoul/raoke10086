﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raoke10086.Service.Models;
namespace Raoke10086.Service.Abstracts
{
    /// <summary>
    /// 商品基本操作服务
    /// </summary>
    public interface IProductSvc: IDependency
    {
        /// <summary>
        /// 获取商品详情
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        ProductDto Get(Guid userID, Guid id);

        /// <summary>
        /// 筛选商品
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        PageList<ProductInListDto> Get(Guid userID, ProductFilterOfPage filter);

        /// <summary>
        /// 更新商品
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="productID"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(Guid userID,Guid? productID, UpdateProductDto model);

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        bool Delete(Guid userID, params Guid[] productIDs);

        /// <summary>
        /// 上架商品
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        bool Listing(Guid userID, params Guid[] productIDs);

        /// <summary>
        /// 下架商品
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        bool Delisting(Guid userID, params Guid[] productIDs);

        /// <summary>
        /// 当前分类供货城市
        /// </summary>
        /// <param name="lcid">LCID</param>
        /// <param name="categaryID">分类ID</param>
        /// <returns></returns>
        List<CityInListDto> GetCity(string lcid, Guid categaryID);

        /// <summary>
        /// 供货城市
        /// </summary>
        /// <param name="lcid">LCID</param>
        /// <returns></returns>
        List<CityInListDto> GetAllCity(string lcid);

        /// <summary>
        /// 获取分类
        /// </summary>
        /// <param name="pid">父ID</param>
        /// <returns></returns>
        List<CategoryInListDto> GetCategory(Guid? pid);

        /// <summary>
        /// 获取标签
        /// </summary>
        /// <returns></returns>
        List<TagDto> GetTags();
    }
}
