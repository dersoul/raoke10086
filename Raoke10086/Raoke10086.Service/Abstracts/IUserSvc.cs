﻿using Raoke10086.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Abstracts
{
    /// <summary>
    /// 用户接口
    /// </summary>
    public interface IUserSvc: IDependency
    {
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <returns></returns>
        UserProfileDto Create(CreateUserDto model);

        /// <summary>
        /// 获取用户资料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UserProfileDto Get(Guid id);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        UserProfileDto Login(LoginDto model);

        /// <summary>
        /// 通过第三方登录
        /// </summary>
        /// <returns></returns>
        UserProfileDto ExternalLogin(ExternalLoginDto model);

        /// <summary>
        /// 更新用户资料
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <param name="model">用户资料</param>
        /// <returns></returns>
        bool UpdateProfile(Guid id, UpdateUserProfileDto model);

        /// <summary>
        /// 锁定用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Lock(Guid id);

        /// <summary>
        /// 解锁用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Unlock(Guid id);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <returns></returns>
        bool Delete(Guid id);
    }
}
