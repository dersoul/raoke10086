﻿using Qiniu.IO;
using Qiniu.RS;
using Raoke10086.Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Service.Concretes
{
    public class ToolkitSvc : IToolkitSvc
    {
        public string GetQiniuToken(string bucket)
        {
            PutPolicy put = new PutPolicy(bucket, 3600);
            return put.Token();
        }
    }
}
