﻿using Raoke10086.Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raoke10086.Service.Models;
using Raoke10086.Service.Entities;
using Raoke10086.Infrastructure.Enums;
using EntityFramework.Extensions;
using EntityFramework.Caching;

namespace Raoke10086.Service.Concretes
{
    public class ProductSvc : IProductSvc
    {
        #region 构造函数注册上下文
        private readonly Raoke10086Context _context;

        public ProductSvc(Raoke10086Context context)
        {
            _context = context;
        }
        #endregion

        public bool Delete(Guid userID, params Guid[] productIDs)
        {
            return _context.Products
                .Where(p => productIDs.Contains(p.ID))
                .Delete() > 0;
        }

        public bool Delisting(Guid userID, params Guid[] productIDs)
        {
            return _context.Products
                  .Where(p => p.State == ProductState.OnSale && productIDs.Contains(p.ID))
                  .Update(p => new Product { State = ProductState.SoldOut }) > 0;
        }

        public ProductDto Get(Guid userID, Guid id)
        {
            var query = from product in _context.Products
                        where product.ID == id
                        select new ProductDto
                        {
                            ID = product.ID,
                            CategaryID = product.CategaryID,
                            Tags = product.Tags,
                            Title = product.Title,
                            Cover = product.Cover,
                            City = product.CityCode,
                            IsVirtual = product.IsVirtual,
                            Messages = product.Messages,
                            ServiceProvider = product.ServiceProvider,
                            DataGalleries = product.Galleries,
                            NoteMessage = product.NoteMessage,
                            OriginPrice = product.OriginPrice,
                            SalePrice = product.SalePrice,
                            PostPrice = product.PostPrice,
                            Stock = product.Stock,
                            Sold = product.Sold,
                            State = product.State,
                            Introduction = product.Introduction,
                            DataDetails = product.Details,
                        };
            return query.FromCacheFirstOrDefault(
                cachePolicy: CachePolicy.WithDurationExpiration(TimeSpan.FromSeconds(7200)),
                tags: new[] { "Product", "Product:" + id });
        }

        public PageList<ProductInListDto> Get(Guid userID, ProductFilterOfPage filter)
        {
            var query = from product in _context.Products
                        join category in _context.Categories on product.CategaryID equals category.ID
                        where (!filter.LowestPrice.HasValue || product.SalePrice >= filter.LowestPrice.Value)
                        && (!filter.HighestPrice.HasValue || product.SalePrice <= filter.HighestPrice.Value)
                        && (string.IsNullOrEmpty(filter.Keywords) || product.Title.Contains(filter.Keywords.Trim()) )
                        orderby product.ID
                        select new ProductInListDto
                        {
                            ID = product.ID,
                            Cover = product.Cover,
                            Title = product.Title,
                            Tags = product.Tags,
                            CategoryID = category.ID,
                            CategoryName = category.Name,
                            Introduction = product.Introduction,
                            OriginPrice = product.OriginPrice,
                            PostPrice = product.PostPrice,
                            SalePrice = product.SalePrice,
                            Sold = product.Sold,
                            Stock = product.Stock,
                            State=product.State
                        };
            var total = query.FutureCount();
            var products = query.Skip((filter.Page - 1) * filter.PageSize).Take(filter.PageSize).Future();
            return new PageList<ProductInListDto>
            {
                TotalCount = total,
                Data = products.ToList(),
                Page = filter.Page,
                PageSize = filter.PageSize,
            };

        }

        public List<CategoryInListDto> GetCategory(Guid? pid)
        {
            var query = from category in _context.Categories
                        where category.PID == (pid ?? Guid.Empty)
                        select new CategoryInListDto
                        {
                            ID = category.ID,
                            Name = category.Name,
                        };
            return query.FromCache(
                cachePolicy: CachePolicy.WithDurationExpiration(TimeSpan.FromSeconds(7200)),
                tags: new[] { "Category" }).ToList();
        }

        public List<CityInListDto> GetCity(string lcid, Guid categaryID)
        {
            var query = from product in _context.Products
                        join city in _context.Cities on new { lcid, code = product.CityCode } equals new { lcid = city.LCID, code = city.Code }
                        where product.CategaryID == categaryID && product.Stock > 0 && product.State == ProductState.OnSale
                        group city by new { city.Code, city.Name, city.FullName } into citys
                        select new CityInListDto
                        {
                            Code = citys.Key.Code,
                            Name = citys.Key.Name,
                            Fullname = citys.Key.FullName
                        };
            return query.ToList();
        }

        public List<CityInListDto> GetAllCity(string lcid)
        {
            return new List<CityInListDto>
            {
                new CityInListDto { Code="1",Fullname="中国·四川·成都",Name="成都" },
                new CityInListDto { Code="2",Fullname="中国·广东·广州",Name="广州" },
            };
        }

        public List<TagDto> GetTags()
        {
            return new List<TagDto>
            {
                #region 运营商标签
                new TagDto
                {
                    ID=1101,
                    Name="移动",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                new TagDto
                {
                    ID=1102,
                    Name="联通",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                new TagDto
                {
                    ID=1103,
                    Name="电信",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                #endregion
                #region 品牌标签
                new TagDto
                {
                    ID=1201,
                    Name="动感地带",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                new TagDto
                {
                    ID=1202,
                    Name="全球通",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                new TagDto
                {
                    ID=1203,
                    Name="神州行",
                    Style="border: 1px solid #AEC1A5;color: #AEC1A5;",
                },
                #endregion
                #region 城市标签
                new TagDto
                {
                    ID=1301,
                    Name="广州",
                    Style="border: 1px solid #9CA1B7;color: #9CA1B7;",
                },
                new TagDto
                {
                    ID=1302,
                    Name="深圳",
                    Style="border: 1px solid #9CA1B7;color: #9CA1B7;",
                },
                new TagDto
                {
                    ID=1303,
                    Name="佛山",
                    Style="border: 1px solid #9CA1B7;color: #9CA1B7;",
                },
                #endregion  



            };
        }

        public bool Listing(Guid userID, params Guid[] productIDs)
        {
            return _context.Products
                   .Where(p => (p.State == ProductState.SoldOut || p.State == ProductState.Unpublished) && productIDs.Contains(p.ID))
                   .Update(p => new Product { State = ProductState.OnSale }) > 0;
        }

        public bool Update(Guid userID, Guid? productID, UpdateProductDto model)
        {
            if (productID.HasValue)
            {
                CacheManager.Current.Expire("Product:" + productID);
                return _context.Products.Where(p => p.ID == productID).Update(p => new Product
                {
                    CityCode = model.City,
                    CategaryID = model.CategaryID,
                    Title = model.Title,
                    OriginPrice = model.OriginPrice,
                    SalePrice = model.SalePrice,
                    PostPrice = model.PostPrice,
                    IsVirtual = model.IsVirtual,
                    Stock = model.Stock,
                    ServiceProvider = model.ServiceProvider,
                    Cover = model.Cover,
                    Galleries = Newtonsoft.Json.JsonConvert.SerializeObject(model.Galleries),
                    NoteMessage = model.NoteMessage,
                    Introduction = model.Introduction,
                    Messages = model.NoteMessage,
                    Tags = model.Tags,
                    Details = model.Details
                }) > 0;
            }
            else
            {
                _context.Products.Add(new Product
                {
                    ID = Guid.NewGuid(),
                    State = ProductState.Unpublished,
                    Sold = 0,

                    CityCode = model.City,
                    CategaryID = model.CategaryID,
                    Title = model.Title,
                    OriginPrice = model.OriginPrice,
                    SalePrice = model.SalePrice,
                    PostPrice = model.PostPrice,
                    IsVirtual = model.IsVirtual,
                    Stock = model.Stock,
                    ServiceProvider = model.ServiceProvider,
                    Cover = model.Cover,
                    Galleries = Newtonsoft.Json.JsonConvert.SerializeObject(model.Galleries),
                    NoteMessage = model.NoteMessage,
                    Introduction = model.Introduction,
                    Messages = model.NoteMessage,
                    Tags = model.Tags,
                    Details = model.Details
                });
                return _context.SaveChanges() > 0;
            }


        }

    }
}
