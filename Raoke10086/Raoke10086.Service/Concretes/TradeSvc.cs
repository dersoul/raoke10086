﻿using Raoke10086.Service.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raoke10086.Service.Models;
using Raoke10086.Service.Entities;
using Raoke10086.Infrastructure.Enums;
using EntityFramework.Extensions;

namespace Raoke10086.Service.Concretes
{
    public class TradeSvc : ITradeSvc
    {
        #region 构造函数注册上下文
        private readonly Raoke10086Context _context;
        private readonly IProductSvc _productSvc;

        public TradeSvc(Raoke10086Context context,IProductSvc productSvc)
        {
            _context = context;
            _productSvc = productSvc;
        }
        #endregion

        public bool Close(Guid userID, Guid id)
        {
            return _context.Trades
                .Where(t => t.BuyerID == userID && t.ID == id
                            && t.State != TradeState.Completed
                            && t.ShipmentState == ShipmentState.Unshipped
                            && t.PayState == PayState.Unpaid)
                .Update(t => new Trade { State = TradeState.Canceled }) > 0;
        }

        public List<TradeDto> Get(Guid userID, TradeFilter filter)
        {
            throw new NotImplementedException();
        }

        public TradeDto Get(Guid userID, Guid id)
        {
            return Get(userID, new TradeFilter { ID = new[] { id } }).FirstOrDefault();
        }

        public TradeDto Create(Guid userID, PostTradeDto model)
        {
            var id = Guid.NewGuid();
            var no = string.Empty;
            var product = _productSvc.Get(userID, model.ProductID);
            var price = (decimal)model.Number * product.SalePrice;
            var postage = product.PostPrice;
            _context.Trades.Add(new Trade
            {
                ID = id,
                No=no,
                ExternalID = model.ExternalID,
                State=TradeState.Unrecognized,
                PayState=PayState.Unpaid,
                ShipmentState=ShipmentState.Unshipped,
                GenerationTime=DateTime.Now,


                #region 商品冗余信息
                ProductID = model.ProductID,
                ProductCover = product.Cover,
                ProductOriginPrice = product.OriginPrice,
                ProductSalePrice = product.SalePrice,
                ProductTitle = product.Title,
                #endregion
                BuyerID=userID,
                ReceiverAddress=model.ReceiverAddress,
                ReceiverName=model.ReceiverName,
                ReceiverPhone=model.ReceiverPhone,

                Number = model.Number,
                Price = price,
                Postage = postage,
                Message = model.Messages,
                Memo = model.Memo,
            });
            return Get(userID, id);
        }

        public bool Receipt(Guid userID, ReceiptDto model)
        {
            throw new NotImplementedException();
        }
    }
}
