﻿using Raoke10086.Service.Abstracts;
using Raoke10086.Service.Entities;
using Raoke10086.Service.Models;
using System;
using Raoke10086.Infrastructure.Enums;
using System.Linq;
using EntityFramework.Extensions;
using EntityFramework.Caching;

namespace Raoke10086.Service.Concretes
{
    public class UserSvc : IUserSvc
    {
        #region 构造函数注册上下文
        private readonly Raoke10086Context _context;

        public UserSvc(Raoke10086Context context)
        {
            _context = context;
        }
        #endregion

        public UserProfileDto Create(CreateUserDto model)
        {
            var userID = Guid.NewGuid();
            _context.Users.Add(new User
            {
                ID = Guid.NewGuid(),
                UserName = model.UserName,
                Email = model.Email,
                EmailConfirmed = model.EmailConfirmed,
                Phone = model.Phone,
                PhoneConfirmed = model.PhoneConfirmed,
                Password = BCrypt.Net.BCrypt.HashPassword(model.Password),
                PayPassword = string.IsNullOrEmpty(model.PayPassword) ? null : BCrypt.Net.BCrypt.HashPassword(model.PayPassword),

                //初始化
                AccessFailedCount = 0,
                LockoutEnabled = true,
                LockoutEndDateUtc = null,
                TwoFactorEnabled = false,
                State = UserState.Activation,
                JoinDate = DateTime.Now,
                Logined = null,
            });
            //用户资料
            _context.UserProfiles.Add(new UserProfile
            {
                UserID = userID,
                Nickname = model.Nickname,
                RealName = model.RealName,
                Avatar = model.Avatar,
                Gender = model.Gender,
                Birthday = model.Birthday,
                Calendar = model.Calendar,
                Career = model.Career,
                Company = model.Company,
                Intro = model.Intro,
            });

            //开通钱包
            _context.Wallets.Add(new Wallet
            {
                ID = userID,
                Value = 0,
                FrozenValue = 0,
            });
            //第三方登录
            if (!string.IsNullOrEmpty(model.LoginProvider) && !string.IsNullOrEmpty(model.ProviderKey))
            {
                _context.UserLogins.Add(new UserLogin
                {
                    UserID = userID,
                    ProviderKey = model.ProviderKey,
                    LoginProvider = model.LoginProvider
                });
            }
            _context.SaveChanges();
            return Get(userID);
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public UserProfileDto ExternalLogin(ExternalLoginDto model)
        {
            return Get(_context.UserLogins.Where(x => x.LoginProvider == model.LoginProvider && x.ProviderKey == model.ProviderKey).Select(x => x.UserID).FirstOrDefault());
        }

        public UserProfileDto Get(Guid id)
        {
            var query = from user in _context.Users
                        join profile in _context.UserProfiles on user.ID equals profile.UserID
                        where user.ID == id
                        select new UserProfileDto
                        {
                            ID = user.ID,
                            UserName = user.UserName,
                            Email = user.Email,
                            Phone = user.Phone,
                            State = user.State,
                            RealName = profile.RealName,
                            Nickname = profile.Nickname,
                            Avatar = profile.Avatar,
                            Gender = profile.Gender,
                            Calendar = profile.Calendar,
                            Birthday = profile.Birthday,
                            Career = profile.Career,
                            Company = profile.Company,
                            Intro = profile.Intro
                        };
            return query.FromCacheFirstOrDefault(
                cachePolicy: CachePolicy.WithDurationExpiration(TimeSpan.FromSeconds(3600)),
                tags: new[] { "UserProfile", "UserProfile:" + id });
        }

        public bool Lock(Guid id)
        {
            return _context.Users
                .Where(u => u.ID == id && u.State == UserState.Activation)
                .Update(u => new User { State = UserState.Lockout, LockoutEndDateUtc = DateTime.MaxValue }) > 0;
        }

        public UserProfileDto Login(LoginDto model)
        {
            var user = (from u in _context.Users
                         where u.EmailConfirmed && u.Email == model.Email
                         select u).FirstOrDefault();
            if (user != null && BCrypt.Net.BCrypt.Verify(model.Password, user.Password))
            {
                return Get(user.ID);
            }
            return null;
        }

        public bool Unlock(Guid id)
        {
            return _context.Users
                .Where(u => u.ID == id && u.State == UserState.Lockout)
                .Update(u => new User { State = UserState.Activation, LockoutEndDateUtc = DateTime.MaxValue }) > 0;
        }

        public bool UpdateProfile(Guid id, UpdateUserProfileDto model)
        {
            return _context.UserProfiles
                 .Where(u => u.UserID == id)
                 .Update(u => new UserProfile
                 {
                     RealName = model.RealName,
                     Nickname = model.Nickname,
                     Avatar = model.Avatar,
                     Gender = model.Gender,
                     Birthday = model.Birthday,
                     Calendar = model.Calendar,
                     Career = model.Career,
                     Company = model.Company,
                     Intro = model.Intro
                 }) > 0;
        }
    }
}
