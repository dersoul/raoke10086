﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 产品状态
    /// </summary>
    public enum ProductState
    {
        /// <summary>
        /// 未发布
        /// </summary>
        Unpublished = 0,

        /// <summary>
        /// 下架
        /// </summary>
        SoldOut = 1,

        /// <summary>
        /// 出售中
        /// </summary>
        OnSale = 2,
    }
}
