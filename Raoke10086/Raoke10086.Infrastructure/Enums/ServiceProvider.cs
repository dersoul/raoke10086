﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 服务提供商
    /// </summary>
    public enum ServiceProvider
    {
        /// <summary>
        /// 本店提供
        /// </summary>
        [Description("本店提供")]
        OurShop=0,

        /// <summary>
        /// 第三方供货商
        /// </summary>
        [Description("第三方供货商")]
        ThirdParties =1,

        /// <summary>
        /// 中国移动
        /// </summary>
        [Description("中国移动")]
        ChinaMobile = 2,

        /// <summary>
        /// 中国联通
        /// </summary>
        [Description("中国联通")]
        ChinaUnicom = 3,

        /// <summary>
        /// 中国电信
        /// </summary>
        [Description("中国电信")]
        ChinaNet = 4,
    }
}
