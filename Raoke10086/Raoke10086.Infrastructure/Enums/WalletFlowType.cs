﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 资金流动类型
    /// </summary>
    public enum WalletFlowType
    {
        /// <summary>
        /// 充值
        /// </summary>
        Recharge,

        /// <summary>
        /// 转账
        /// </summary>
        Transfer,

        /// <summary>
        /// 购物
        /// </summary>
        Shopping,

        /// <summary>
        /// 奖励
        /// </summary>
        Reward,

        /// <summary>
        /// 惩罚
        /// </summary>
        Punishment
    }
}
