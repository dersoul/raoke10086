﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    public enum TradeState
    {
        /// <summary>
        /// 未确认
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// 已确认
        /// </summary>
        Recognized = 1,

        /// <summary>
        /// 完成
        /// </summary>
        Completed = 2,

        /// <summary>
        /// 用户取消
        /// </summary>
        Canceled = 3,

        /// <summary>
        /// 无效订单
        /// </summary>
        Invalid = 4,
    }
}
