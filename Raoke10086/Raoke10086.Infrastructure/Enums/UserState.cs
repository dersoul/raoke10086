﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    public enum UserState
    {
        /// <summary>
        /// 未激活
        /// </summary>
        Inaction = 0,

        /// <summary>
        /// 被锁定
        /// </summary>
        Lockout = 1,

        /// <summary>
        /// 激活
        /// </summary>
        Activation = 2,
    }
}
