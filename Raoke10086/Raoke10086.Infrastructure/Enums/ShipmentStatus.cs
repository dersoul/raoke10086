﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raoke10086.Infrastructure.Enums
{
    /// <summary>
    /// 配送状态
    /// </summary>
    public enum ShipmentState
    {
        /// <summary>
        /// 未发货
        /// </summary>
        Unshipped = 0,

        /// <summary>
        /// 已发货
        /// </summary>
        Shipped = 1,

        /// <summary>
        /// 已收货
        /// </summary>
        Received = 2,

        /// <summary>
        /// 退货中
        /// </summary>
        Returning = 5,

        /// <summary>
        /// 已退货
        /// </summary>
        Returned = 6,
    }
}
